---- Minecraft Crash Report ----
// Hi. I'm Minecraft, and I'm a crashaholic.

Time: 10/8/20, 10:34 PM
Description: Initializing game

java.lang.ExceptionInInitializerError
	at net.minecraft.client.options.GameOptions.<init>(GameOptions.java:218)
	at net.minecraft.client.MinecraftClient.<init>(MinecraftClient.java:409)
	at net.minecraft.client.main.Main.main(Main.java:149)
	at java.base/jdk.internal.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at java.base/jdk.internal.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at java.base/jdk.internal.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.base/java.lang.reflect.Method.invoke(Method.java:564)
	at net.fabricmc.loader.game.MinecraftGameProvider.launch(MinecraftGameProvider.java:227)
	at net.fabricmc.loader.launch.knot.Knot.init(Knot.java:140)
	at net.fabricmc.loader.launch.knot.KnotClient.main(KnotClient.java:26)
	at net.fabricmc.devlaunchinjector.Main.main(Main.java:86)
Caused by: java.lang.RuntimeException: java.lang.UnsatisfiedLinkError: Failed to locate library: lwjgl.dll
	at net.minecraft.client.util.InputUtil.<clinit>(InputUtil.java:108)
	... 11 more
Caused by: java.lang.UnsatisfiedLinkError: Failed to locate library: lwjgl.dll
	at org.lwjgl.system.Library.loadSystem(Library.java:147)
	at org.lwjgl.system.Library.loadSystem(Library.java:67)
	at org.lwjgl.system.Library.<clinit>(Library.java:50)
	at org.lwjgl.system.MemoryAccessJNI.<clinit>(MemoryAccessJNI.java:13)
	at org.lwjgl.system.Pointer.<clinit>(Pointer.java:28)
	at org.lwjgl.system.Platform.mapLibraryNameBundled(Platform.java:80)
	at org.lwjgl.glfw.GLFW.<clinit>(GLFW.java:674)
	at java.base/jdk.internal.misc.Unsafe.ensureClassInitialized0(Native Method)
	at java.base/jdk.internal.misc.Unsafe.ensureClassInitialized(Unsafe.java:1140)
	at java.base/java.lang.invoke.DirectMethodHandle$EnsureInitialized.computeValue(DirectMethodHandle.java:359)
	at java.base/java.lang.invoke.DirectMethodHandle$EnsureInitialized.computeValue(DirectMethodHandle.java:356)
	at java.base/java.lang.ClassValue.getFromHashMap(ClassValue.java:226)
	at java.base/java.lang.ClassValue.getFromBackup(ClassValue.java:208)
	at java.base/java.lang.ClassValue.get(ClassValue.java:114)
	at java.base/java.lang.invoke.DirectMethodHandle.checkInitialized(DirectMethodHandle.java:380)
	at java.base/java.lang.invoke.DirectMethodHandle.ensureInitialized(DirectMethodHandle.java:370)
	at java.base/java.lang.invoke.DirectMethodHandle.ensureInitialized(DirectMethodHandle.java:403)
	at net.minecraft.client.util.InputUtil.<clinit>(InputUtil.java:105)
	... 11 more


A detailed walkthrough of the error, its code path and all known details is as follows:
---------------------------------------------------------------------------------------

-- Head --
Thread: Render thread
Stacktrace:
	at net.minecraft.client.options.GameOptions.<init>(GameOptions.java:218)
	at net.minecraft.client.MinecraftClient.<init>(MinecraftClient.java:409)

-- Initialization --
Details:
Stacktrace:
	at net.minecraft.client.main.Main.main(Main.java:149)
	at java.base/jdk.internal.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at java.base/jdk.internal.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at java.base/jdk.internal.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.base/java.lang.reflect.Method.invoke(Method.java:564)
	at net.fabricmc.loader.game.MinecraftGameProvider.launch(MinecraftGameProvider.java:227)
	at net.fabricmc.loader.launch.knot.Knot.init(Knot.java:140)
	at net.fabricmc.loader.launch.knot.KnotClient.main(KnotClient.java:26)
	at net.fabricmc.devlaunchinjector.Main.main(Main.java:86)

-- System Details --
Details:
	Minecraft Version: 1.16.3
	Minecraft Version ID: 1.16.3
	Operating System: Windows 10 (amd64) version 10.0
	Java Version: 14.0.1, AdoptOpenJDK
	Java VM Version: OpenJDK 64-Bit Server VM (mixed mode, sharing), AdoptOpenJDK
	Memory: 690717760 bytes (658 MB) / 1482686464 bytes (1414 MB) up to 8573157376 bytes (8176 MB)
	CPUs: 16
	JVM Flags: 4 total; -Xdebug -Xnoagent -Xrunjdwp:transport=dt_socket,address=localhost:61520,server=n,suspend=y -XX:+ShowCodeDetailsInExceptionMessages
	Launched Version: Fabric
	Backend library: LWJGL version 3.2.2 build 10
	Backend API: ~~ERROR~~ NoClassDefFoundError: Could not initialize class org.lwjgl.glfw.GLFW
	GL Caps: 
	Using VBOs: Yes
	Is Modded: Definitely; Client brand changed to 'fabric'
	Type: Client (map_client.txt)
	CPU: <unknown>