package com.biom4st3r.position_leak_patch.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;
import net.minecraft.entity.EntityType;
import net.minecraft.network.Packet;
import net.minecraft.network.packet.s2c.play.EntityPositionS2CPacket;
import net.minecraft.server.network.ServerPlayNetworkHandler;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.math.Vec3d;

@Mixin(ServerPlayNetworkHandler.class)
public abstract class ServerPlayerNetworkHandlerMxn {
    @Shadow
    public ServerPlayerEntity player;
    @Inject(
        at = @At("HEAD"),
        method = "sendPacket(Lnet/minecraft/network/Packet;Lio/netty/util/concurrent/GenericFutureListener;)V",
        cancellable = true,
        locals = LocalCapture.NO_CAPTURE)
    public void CHANGEMEAAAA(Packet<?> p, GenericFutureListener<? extends Future<? super Void>> listener,CallbackInfo ci)
    {
        if(p instanceof EntityPositionS2CPacket)
        {
            EntityPosPacketS2CPacketAccess packet =  (EntityPosPacketS2CPacketAccess) p;
            boolean inRange = player.getPos().isInRange(new Vec3d(packet.$getX(), packet.$getY(), packet.$getZ()), EntityType.PLAYER.getMaxTrackDistance()*16);
            if(!inRange)
            {
                EntityPosPacketS2CPacketAccess access = (EntityPosPacketS2CPacketAccess) packet;
                access.setX(player.getRandom().nextInt(253817)-100000);
                access.setY(player.getRandom().nextInt(100)+8);
                access.setZ(player.getRandom().nextInt(253817)-100000);
            }
        }
    }
}
