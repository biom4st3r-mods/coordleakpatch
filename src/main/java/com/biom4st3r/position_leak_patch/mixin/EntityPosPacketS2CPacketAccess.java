package com.biom4st3r.position_leak_patch.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

import net.minecraft.network.packet.s2c.play.EntityPositionS2CPacket;

@Mixin(EntityPositionS2CPacket.class)
public interface EntityPosPacketS2CPacketAccess {
    @Accessor("x")
    public void setX(double x);
    @Accessor("y")
    public void setY(double y);
    @Accessor("z")
    public void setZ(double z);
    @Accessor("x")
    public double $getX();
    @Accessor("y")
    public double $getY();
    @Accessor("z")
    public double $getZ();
}
